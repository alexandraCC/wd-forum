<?php
require '../initialize.php';

session_start();

unset($_SESSION['id']);
unset($_SESSION['name']);

session_destroy();

return json_encode(['success' => true]);
