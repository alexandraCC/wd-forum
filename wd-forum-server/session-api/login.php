<?php

require '../initialize.php';
include '../items/User.php';

$username = $_POST['username'];
$password = $_POST['password'];

if (!$username || !$password) {
	echo json_encode(['error' => true, 'errorMessage' => 'All the fields needs to be fulfilled!']);
	exit();
}

$response = (new User())->authenticateUser($readOnlyDBConnection, $username, $password);

if ($response['success']) {
	session_start();

	$_SESSION['userId'] =  $response['userId'];
	$response['sessionId'] = session_id();
}

echo json_encode($response);
