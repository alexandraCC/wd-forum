<?php

require '../initialize.php';
require '../validate-session.php';
include '../items/Post.php';

$title = $_POST['title'];
$text = $_POST['text'];
$tag = $_POST['tag'];
$userId = $_POST['userId'];
$token = $_POST['token'];

if ($token !== $_SESSION['csrf_token']) {
	echo json_encode(['error' => true, 'errorMessage' => 'Permission denied!']);
	exit();
}

if (!$title || !$text || !$tag || !$userId) {
	echo json_encode(['error' => true, 'errorMessage' => 'All the fields needs to be fulfilled!']);
	exit();
}

if (isset($_FILES['file'])) {
	if (!getimagesize($_FILES['file']['tmp_name']) || $_FILES['file']['size'] > 500000) {
		echo json_encode(['error' => true, 'errorMessage' => 'Something went wrong when uploading the file, try again!']);
		exit();
	}
	
	$encodedFileContents = base64_encode(file_get_contents($_FILES['file']['tmp_name']));
	$response = (new Post())->createPost($CRUDDBConnection, $title, $text, $tag, $userId, $encodedFileContents);
	echo json_encode($response);
	exit();
}

$response = (new Post())->createPost($CRUDDBConnection, $title, $text, $tag, $userId);
echo json_encode($response);
