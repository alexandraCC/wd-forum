<?php

require '../initialize.php';
include '../items/Post.php';

$response = (new Post())->getPosts($CRUDDBConnection);

echo json_encode($response);
