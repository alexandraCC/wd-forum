<?php

require '../initialize.php';
require '../validate-session.php';
include '../items/Comment.php';

$text = $_POST['text'];
$userId = $_POST['userId'];
$postId = $_POST['postId'];
$token = $_POST['token'];

if ($token !== $_SESSION['csrf_token']) {
	echo json_encode(['error' => true, 'errorMessage' => 'Permission denied!']);
	exit();
}

if (!$text || !$userId || !$postId) {
	echo json_encode(['error' => true, 'errorMessage' => 'All the fields needs to be fulfilled!']);
	exit();
}

if (isset($_FILES['file'])) {
	if (!getimagesize($_FILES['file']['tmp_name']) || $_FILES['file']['size'] > 500000) {
		echo json_encode(['error' => true, 'errorMessage' => 'Something went wrong when uploading the file, try again!']);
		exit();
	}
	
	$encodedFileContents = base64_encode(file_get_contents($_FILES['file']['tmp_name']));
	$response = (new Comment())->createComment($CRUDDBConnection, $text, $userId, $postId, $encodedFileContents);
	echo json_encode($response);
	exit();
}

$response = (new Comment())->createComment($CRUDDBConnection, $text, $userId, $postId);
echo json_encode($response);
