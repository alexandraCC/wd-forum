<?php

require '../initialize.php';
require '../validate-session.php';
include '../items/Diploma.php';

$userId = $_POST['userId'];

if ($userId !== $_SESSION['userId']) {
	echo json_encode(['error' => true, 'errorMessage' => 'You do not have permission for this action!']);
	exit();
}

$response = (new Diploma())->getDiplomasForUser($CRUDDBConnection, $userId);

echo json_encode($response);
