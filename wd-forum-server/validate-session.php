<?php
session_start();

if (!isset($_REQUEST['allowWithoutSession']) && (!isset($_REQUEST['sessionId']) || $_REQUEST['sessionId'] !== session_id())) {
	echo json_encode(['error' => true, 'errorMessage' => 'You do not have permission for this action!']);
	exit();
}
