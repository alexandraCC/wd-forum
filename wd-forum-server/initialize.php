<?php

error_reporting(E_ALL);
ini_set('ignore_repeated_errors', TRUE);
ini_set('display_errors', FALSE);
ini_set('log_errors', TRUE);
ini_set('error_log', __DIR__.'/debug.log');

require 'vendor/autoload.php';
require_once ('database/DatabaseConnection.php');

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$http_origin = $_SERVER['HTTP_ORIGIN'];
if (in_array($http_origin, ['http://localhost:8082', 'http://206.81.31.202'])) {
	header('Access-Control-Allow-Origin: ' . $http_origin);
}

header('Access-Control-Allow-Credentials: true');

$readOnlyDBConnection = (new DatabaseConnection('read_only'))->getConnection();
$CRUDDBConnection = (new DatabaseConnection('CRUD_permissions'))->getConnection();
