<?php

final class User {
	private $salt;
	
	private static function getPepper(): string {
		return 'MWQxNTY1YTctMmVhYS00YmRhLWE3OGEtMDNhZDFmYTc1ZGRm';
	}
	
	private function hashPassword($password): string {
		$this->salt = base64_encode(random_bytes(16));
		return hash('sha256',$password.$this->salt.$this->getPepper());
	}
	
	public function createUser($db, $firstName, $lastName, $userName, $email, $password): array {
		try {
			$q = $db->prepare('INSERT INTO users VALUES(null, :first_name, :last_name, :username, :email, :salt, :hash, 0, 0)');
			$q->bindValue(':first_name', $firstName);
			$q->bindValue(':last_name', $lastName);
			$q->bindValue(':username', $userName);
			$q->bindValue(':email', $email);
			$q->bindValue(':hash', base64_encode($this->hashPassword($password)));
			$q->bindValue(':salt', $this->salt);
			$q->execute();

			return ['success' => true, 'id' => $db->lastInsertId()];
		} catch (Exception $e) {
			return ['error' => true, 'errorMessage' => 'Creating a new user has failed!'];
		}
	}
	
	public function validateUser($db, $id): array {
		try {
			$q = $db->prepare('UPDATE users SET verified = :verified WHERE id = :id');
			$q->bindValue(':id', $id);
			$q->bindValue(':verified', true);
			
			$q->execute();
			return ['success' => true, 'message' => 'Updated user with id: '.$id];
		} catch (PDOException $ex) {
			return ['error' => true, 'errorMessage' => 'Updating user failed'];
		}
	}
	
	public function updateUserToPremium($db, $id): array {
		try {
			$q = $db->prepare('UPDATE users SET premium = :premium WHERE id = :id');
			$q->bindValue(':id', $id);
			$q->bindValue(':premium', true);
			
			$q->execute();
			return ['success' => true, 'message' => 'Updated user with id: '.$id];
		} catch (PDOException $ex) {
			return ['error' => true, 'errorMessage' => 'Updating user failed'];
		}
	}
	
	public function authenticateUser($db, $username, $password): array {
		try {
			$q = $db->prepare('SELECT * FROM users WHERE username = :username');
			$q->bindValue(':username', $username);
			$q->execute();
			
			$user = $q->fetch();
			
			if (!$user) {
				return ['error' => true, 'errorMessage' => 'Invalid credentials!'];
			}
			
			if (!$user->verified) {
				return ['error' => true, 'errorMessage' => 'Please verify your user first! Check your email to find the verification link!'];
			}
			
			$hash = base64_encode(hash('sha256',$password.$user->salt.self::getPepper()));
			
			if ($hash === $user->hash) {
				return ['success'=> true, 'userId' => $user->id];
			}
			
			return ['error' => true, 'errorMessage' => 'Invalid credentials!'];
		} catch (Exception $e) {
			return ['error' => true, 'errorMessage' => 'Could not authenticate user!'];
		}
	}
	
	public function getUserFromId($db, $userId): array {
		try {
			$q = $db->prepare('SELECT * FROM users WHERE id = :id');
			$q->bindValue(':id', $userId);
			$q->execute();
			
			$user = $q->fetch();
			
			if (!$user) {
				return ['error' => true, 'errorMessage' => 'Invalid user id!'];
			}
			
			return ['success' => true, 'user' => $user];
		} catch (Exception $e) {
			return ['error' => true, 'errorMessage' => 'Could not get user!'];
		}
	}
	
	public function encryptAndStoreCardInfo($db, $userId, $cardNumber, $cardName, $month, $year, $cvvNumber): array {
		$alg = 'aes-256-cbc';
		$key = 'MWQxNTY1YTctMmVhYS00YmRhLWE3OGEtMDNhZDFmYTc1ZGRm';
		$messageToEncrypt = json_encode([
			'cardNumber' => $cardNumber,
			'cardName' => $cardName,
			'month' => $month,
			'year' => $year,
			'cvvNumber' => $cvvNumber,
		]);
		
		$ivlen = openssl_cipher_iv_length($alg);
		$iv = openssl_random_pseudo_bytes($ivlen);
		$encryptedText = openssl_encrypt($messageToEncrypt, $alg, $key, $options=0, $iv);
		
		try {
			$q = $db->prepare('INSERT INTO cardsInfo VALUES(null, :encrypted_info, :iv, :user_id)');
			$q->bindValue(':encrypted_info', $encryptedText);
			$q->bindValue(':iv', base64_encode($iv));
			$q->bindValue(':user_id', $userId);
			$q->execute();
			
			return ['success' => true, 'id' => $db->lastInsertId()];
		} catch (Exception $e) {
			return ['error' => true, 'errorMessage' => 'Creating a card info has failed!'];
		}
	}
	
	public function fetchAndDecryptCardInfo($db, $userId): array {
		$alg = 'aes-256-cbc';
		$key = 'MWQxNTY1YTctMmVhYS00YmRhLWE3OGEtMDNhZDFmYTc1ZGRm';
		
		try {
			$q = $db->prepare('SELECT * FROM cardsInfo WHERE user_id = :user_id');
			$q->bindValue(':user_id', $userId);
			$q->execute();
			
			$cardInfo = $q->fetch();
			
			if (!$cardInfo) {
				return ['error' => true, 'errorMessage' => 'Invalid user id!'];
			}
			
			$originalText = openssl_decrypt($cardInfo->encrypted_info, $alg, $key, $options=0, base64_decode($cardInfo->iv));
			var_dump($originalText);

			return ['success' => true];
		} catch (Exception $e) {
			return ['error' => true, 'errorMessage' => 'Could not authenticate user!'];
		}
	}
	
	public function updateUserInfo($db, $firstName, $lastName, $username, $email, $userId): array {
		try {
			$q = $db->prepare('UPDATE users SET first_name=:first_name, last_name=:last_name, username=:username, email=:email WHERE id=:userId');
			$q->bindValue(':last_name', $lastName);
			$q->bindValue(':first_name', $firstName);
			$q->bindValue(':username', $username);
			$q->bindValue(':email', $email);
			$q->bindValue(':userId', $userId);
			$q->execute();
			
			return ['success' => true];
		} catch (Exception $e) {
			return ['error' => true, 'errorMessage' => 'Could not update user!'];
		}
	}
	
	public function isUserPremium($db, $userId): array {
		try {
			$q = $db->prepare('SELECT * FROM users WHERE id = :id');
			$q->bindValue(':id', $userId);
			$q->execute();
			
			$user = $q->fetch();
			
			return ['success' => true, 'isPremium' => $user->premium];
		} catch (Exception $e) {
			return ['error' => true, 'errorMessage' => 'Could not get user!'];
		}
	}
}
