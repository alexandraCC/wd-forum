<?php

final class Diploma {
	public function createDiploma($db, $answeredQuizId): array {
		try {
			$db->beginTransaction();
			// get answeredQuiz where id is id passed to the
			$answeredQuizzesQ = $db->prepare('SELECT id, user_id, quiz_id FROM answeredQuizzes WHERE id = :id ');
			$answeredQuizzesQ->bindValue(':id', $answeredQuizId);
			$answeredQuizzesQ->execute();

			$answeredQuiz = $answeredQuizzesQ->fetch();
			
			//use quiz_id to get topic_id
			$topicQ = $db->prepare('SELECT id FROM topics WHERE quiz_id = :id ');
			$topicQ->bindValue(':id', $answeredQuiz->quiz_id);
			$topicQ->execute();

			$topic = $topicQ->fetch();
			
			//add diploma to the db
			$diplomaQ = $db->prepare('INSERT INTO diplomas VALUES (NULL, :userId, :topicId, :answeredQuizId, NULL)');
			$diplomaQ->bindValue(':userId', $answeredQuiz->user_id);
			$diplomaQ->bindValue(':topicId', $topic->id);
			$diplomaQ->bindValue(':answeredQuizId', $answeredQuiz->id);

			$diplomaQ->execute();

			$db->commit();
			
			return ['success' => true, 'diplomaId' => $db->lastInsertId()];
		} catch (Exception $e) {
			return ['error' => true, 'errorMessage' => 'Could not create new diploma!'];
		}
	}

	public function getDiplomasForUser($db, $userId): array {
		try {
			$q = $db->prepare('SELECT * FROM diplomas INNER JOIN topics ON diplomas.topic_id = topics.id INNER JOIN answeredQuizzes ON diplomas.answered_quiz_id = answeredQuizzes.id WHERE diplomas.user_id = :id');
			$q->bindValue(':id', $userId);
			$q->execute();

			$data = $q->fetchAll();

			$diplomas = [];
			foreach ($data as $key => $diploma) {
				$diplomas[$key] = [
					'name' => $diploma->name,
					'description' => $diploma->description,
					'quizScore' => $diploma->quiz_score,
				];
			}

			return ['success' => true, 'diplomas' => $diplomas];
		} catch (PDOException $ex) {
			return ['error' => true, 'errorMessage' => 'Could not fetch diplomas!'];
		}
	}
}
