<?php

final class Topic {
	public function getAllTopics($db): array {
		try {
			$q = $db->prepare('SELECT * FROM topics');
			$q->execute();
			
			$topics = $q->fetchAll();
			return ['success' => true, 'topics' => $topics];
		} catch(Exception $e) {
			return ['error' => true, 'errorMessage' => 'Could not fetch topics'];
		}
	}
}
