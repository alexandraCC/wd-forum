<?php

final class Post {

	public function createPost($db, $title, $text, $tag, $userId, $encodedFileContents = null): array {
		try {
			$q = $db->prepare('INSERT INTO posts VALUES(null, :title, :text, :tag, :file_contents, :user_id)');
			$q->bindValue(':title', $title);
			$q->bindValue(':text', $text);
			$q->bindValue(':tag', $tag);
			$q->bindValue(':file_contents', $encodedFileContents);
			$q->bindValue(':user_id', $userId);
			$q->execute();

			return ['success' => true, 'id' => $db->lastInsertId()];
		} catch (Exception $e) {
			return ['error' => true, 'errorMessage' => 'Creating a new post has failed!'];
		}
	}
	
	public function getPosts($db): array {
		try {
			$q = $db->prepare('SELECT * FROM posts');
			$q->execute();
			
			return ['success' => true, 'posts' => $q->fetchAll()];
		} catch (Exception $e) {
			return ['error' => true, 'errorMessage' => 'Could not fetch posts!'];
		}
	}
	
	public function getPost($db, $postId): array {
		try {
			$q = $db->prepare('SELECT * FROM posts WHERE id=:id');
			$q->bindValue(':id', $postId);
			$q->execute();
			
			return ['success' => true, 'post' => $q->fetch()];
		} catch (Exception $e) {
			return ['error' => true, 'errorMessage' => 'Could not fetch posts!'];
		}
	}
}
