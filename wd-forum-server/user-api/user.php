<?php

require '../initialize.php';
require '../validate-session.php';
include '../items/User.php';

$userId = $_POST['userId'];

if (!$userId) {
	echo json_encode(['error' => true, 'errorMessage' => 'Please provide a userId!']);
	exit();
}

if ($userId !== $_SESSION['userId']) {
	echo json_encode(['error' => true, 'errorMessage' => 'You do not have permission for this action!']);
	exit();
}

$response = (new User())->getUserFromId($readOnlyDBConnection, $userId);

echo json_encode($response);
