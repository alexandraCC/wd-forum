<?php

require '../initialize.php';
include '../items/User.php';

$id = $_GET['id'];

$response = (new User())->validateUser($CRUDDBConnection, $id);

if ($response['success']) {
	header('Location: ' . $_ENV['REDIRECT_LOCATION'] );
	exit();
}

echo json_encode(['error' => true, 'errorMessage' => 'Validation has failed!']);
