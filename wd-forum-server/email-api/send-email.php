<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require '../initialize.php';

$subject = $_POST['subject'];
$message = $_POST['message'];
$emailAddressTo = $_POST['emailTo'];
$replyEmail = $_POST['replyEmail'];
$emailFrom = $_POST['emailFrom'];
$name = $_POST['name'];

$mail = new PHPMailer(true);

try {
	//Server settings
	$mail->SMTPDebug = 0;
	$mail->isSMTP();
	$mail->Host = 'smtp.gmail.com';
	$mail->SMTPAuth = true;
	$mail->Username = 'elearning.dbcamp@gmail.com';
	$mail->Password = $_ENV['SMTP_PASSWORD'];
	$mail->SMTPSecure = 'tls';
	$mail->Port = 587;
	
	//Recipients
	$mail->setFrom($emailFrom, $name);
	$mail->addAddress($emailAddressTo, '');
	$mail->addReplyTo($replyEmail, '');
	
	// Content
	$mail->isHTML(true);
	$mail->Subject = $subject;
	$mail->Body = $message;
	
	$mail->send();

	echo json_encode(['success' => true]);
	exit();
} catch (Exception $e) {
	echo json_encode(['error' => true, 'errorMessage' => 'Message could not be sent']);
	exit();
}
