# WDForum
---

## How to get started?
---
1. Have MAMP installed and make sure that it is running on a 7.4 PHP version
2. Clone the repository inside the htdocs folder
3. Make sure you have npm installed
---

#### Server
``` 
cd wd-forum-server
composer install
```
---

#### Client
```
cd wd-forum-client
npm install
npm run serve
```
---

#### Documentation
```
gem install bundler
bundle exec jekyll serve
```

