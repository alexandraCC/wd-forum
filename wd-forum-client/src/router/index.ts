import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

import MyAccount from '@/views/MyAccount.vue';
import AddNewPost from '@/views/AddNewPost.vue';
import EditProfile from '@/views/EditProfile.vue';
import Login from '../views/Login.vue';
import Signup from '../views/Signup.vue';
import Forum from '../views/Forum.vue';
import ForumPost from '../views/ForumPost.vue';
import Courses from '../views/Courses.vue';
import RelationalDB from '../views/topics/RelationalDB.vue';
import WebFrontEnd from '../views/topics/WebFrontEnd.vue';
import InstallRelationalDB from '../views/topics/InstallRelationalDB.vue';
import SQL from '../views/topics/SQL.vue';
import EntityDiagram from '../views/topics/EntityDiagram.vue';
import DBNormalization from '../views/topics/DBNormalization.vue';
import Quiz from '../views/Quiz.vue';
import Home from '../views/Home.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/sign-up',
    name: 'Signup',
    component: Signup,
  },
  {
    path: '/forum',
    name: 'Forum',
    component: Forum,
  },
  {
    path: '/forum-post/:id',
    name: 'ForumPost',
    component: ForumPost,
  },
  {
    path: '/new-post',
    name: 'AddNewPost',
    component: AddNewPost,
  },
  {
    path: '/my-account',
    name: 'MyAccount',
    component: MyAccount,
  },
  {
    path: '/edit-profile',
    name: 'EditProfile',
    component: EditProfile,
  },
  {
    path: '/courses',
    name: 'Courses',
    component: Courses,
  },
  {
    path: '/learn-relational-database',
    name: 'RelationalDB',
    component: RelationalDB,
  },
  {
    path: '/learn-web-front-end',
    name: 'WebFrontEnd',
    component: WebFrontEnd,
  },
  {
    path: '/learn-how-to-install-a-relational-database',
    name: 'InstallRelationalDB',
    component: InstallRelationalDB,
  },
  {
    path: '/learn-sql',
    name: 'SQL',
    component: SQL,
  },
  {
    path: '/learn-entity-diagram',
    name: 'EntityDiagram',
    component: EntityDiagram,
  },
  {
    path: '/learn-database-normalisation',
    name: 'dbNormalization',
    component: DBNormalization,
  },
  {
    path: '/quiz/:id',
    name: 'quiz',
    component: Quiz,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
