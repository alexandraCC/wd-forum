---
layout: default
title: Welcome
---

# WD Forum

WD Forum is a web application for web developers where they can gather online and get help or help others.  
Users can ask questions to the forum where web developers from all around the World can come to the rescue and provide the user with answers or ideas for solutions to codeing problems.
The premium edition of WD Forum includes different learning materials on various web development subjects.

This is the documentation of WD Forum for web developers within the company. This documents how the system work, how it's structured and how to get started developing. 


## Dependencies

* MAMP
* PHP 7.4
* NPM
* Vue.js 3
* VSCode
* MySQL
* Git

## How to get started?

1. Have MAMP installed and make sure that it is running on a 7.3 PHP version
2. Clone the repository from <a href="https://gitlab.com/alexandraCC/wd-forum">GitLab</a> inside the htdocs folder
3. Make sure you have npm installed


### Quick start: Server
```
cd wd-forum-server
```
 
```composer
composer install
```

### Quick start: Client
```
cd wd-forum-client
```

```npm
npm install
```
 
```npm
npm run serve
```