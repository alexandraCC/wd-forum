---
layout: default
title: API
---

## Overview of API's
* diploma-api
    * get-diploma.php
* email-api
    * send-email.php
* forum-api
    * create-comment.php
    * create-post.php
    * get-comments-for-post.php
    * get-post.php
    * get-posts.php
* quiz-api
    * get-count-of-answered-quizzes.php
    * get-quiz.php
    * save-answered-quiz.php
* session-api
    * generate-csrf-token.php
    * login.php
    * logout.php
* topics-api
    * topics.php
* user-api
    * signup.php
    * update-to-premium.php
    * user.php
    * validate.php

## How the APIs are structered
For each of the database entities a PHP item class with the same name exists together with a dedicated API with endpoints for each operation. This approach keeps the structure clear and also ensures separation of concerns, so each item and each endpoint handles dedicated functionality. 
Code reuse is also used on the backend in situations where the same functionality is required in multiple places. An example of code reuse is the session validation, the logic doing it lives in a php file which gets imported in all the endpoints which require authentication.

## How to create a new API
1. Create a folder for your new API inside the wd-forum-server. _example:_ _sms-api_
2. Create the PHP file for your API inside the folder. It is important to give it a logical name. _example:_ _send-sms.php_
3. Start the php tag and require the initialize.php file to get all the basic functionality needed inside the endpoint such as: database connections, CORS headers etc. _example:_
```php
    <?php
    require '../initialize.php';
    ...
```
4. For functions that require user validation, remember to require the validate-session.php. _example:_
```php
    ... 
    require '../validate-session.php';
    ...
```
You are now ready to code your API.
