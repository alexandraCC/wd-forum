---
layout: default
title: Database
---

## ER Diagram
In the picture below you see the ERD of WD Froum. This should give a clear overview of the connections inside the database. 
We aim to have a normalized database with consistent data and therefore it's important to have this in mind while working in the database. 
The existing relations inside our database are one-to-one and one-to-many.

<img src="{{site.baseurl}}/doc_assets/img/erd-wdforum.png">

## How to enter the database
To manage the MySQL database locally, phpMyAdmin is used. Since MAMP is the tool of choice for local development phpMyAdmin is already a part of the tool set.
To access the database via phpMyAdmin, run the server and go to localhost/phpmyadmin.

The database connection is created with PDO (PHP Data Objects) and is located inside the DatabaseConnection.php file in the wd-forum-server/database folder.
