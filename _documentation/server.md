---
layout: default
title: Server
---
For the server-side code PHP 7.4 are used because of the ease of use when talking about creating CRUD APIs and connections to the database.

```
cd wd-forum-server
```
 
```composer
composer install
```

### Table of contents
* <a href="{{site.baseurl}}/documentation/database/">Database</a>
* <a href="{{site.baseurl}}/documentation/api/">API</a>