---
layout: default
title: Components
---

Components are reusable Vue instances with a name. We can use this component as a custom element inside a root Vue instance created with "new Vue".

Components accept options like data, computed, watch, methods and more.

## Reusing components
Components can be reused as many times as you want by following these step:
1. Add the component tag inside the file where you want it to be added. _Example:_
```html
    <div class="demo">
      <TopicBox></TopicBox>
    </div>
```

2. Import the component in the script. _Example:_
```ts
import TopicBox from '@/components/topics/TopicBox.vue';
```

3. Define the component. _Example:_
```ts
export default defineComponent({
  name: 'Courses',
  components: {
    TopicBox,
  },
  ...
})
```
You can see all the created components in the project inside the components folder. 

## When to create a component
You must create a component when a Vue instance should be reused. It is important in order to have a DRY code base. Are you you using the same UI for multiple sites? Please make a component. 

For eksample, we are using the same UI for all the courses of each topic, and therefore a component with the name of "TopicPage" are created and can easily be reused on multiple pages. 

## How to create a component
1. Go to the components folder inside the src folder

2. Create the Vue file for the component

3. Use it as mentioned above in the section "Reusing components"

### More information about creating Vue components:
See the Vue documentation <a href="https://vuejs.org/v2/guide/components.html">on their website</a>.